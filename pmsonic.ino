#include <sps30.h>

// Example arduino sketch, based on 
// https://github.com/Sensirion/embedded-sps/blob/master/sps30-i2c/sps30_example_usage.c

// pwm buzzer
const int buzzer = 9;

// uncomment the next line to use the serial plotter
//#define PLOTTER_FORMAT

int over_time;

void setup() {
  int16_t ret;
  uint8_t auto_clean_days = 4;
  uint32_t auto_clean;

  Serial.begin(9600);
  delay(2000);

  sensirion_i2c_init();

  while (sps30_probe() != 0) {
    Serial.print("SPS sensor probing failed\n");
    delay(500);
  }

#ifndef PLOTTER_FORMAT
  Serial.print("SPS sensor probing successful\n");
#endif /* PLOTTER_FORMAT */

  ret = sps30_set_fan_auto_cleaning_interval_days(auto_clean_days);
  if (ret) {
    Serial.print("error setting the auto-clean interval: ");
    Serial.println(ret);
  }

  ret = sps30_start_measurement();
  if (ret < 0) {
    Serial.print("error starting measurement\n");
  }

#ifndef PLOTTER_FORMAT
  Serial.print("measurements started\n");
#endif /* PLOTTER_FORMAT */

#ifdef SPS30_LIMITED_I2C_BUFFER_SIZE
  Serial.print("Your Arduino hardware has a limitation that only\n");
  Serial.print("  allows reading the mass concentrations. For more\n");
  Serial.print("  information, please check\n");
  Serial.print("  https://github.com/Sensirion/arduino-sps#esp8266-partial-legacy-support\n");
  Serial.print("\n");
  delay(2000);
#endif

  delay(1000);

  // pwm buzzer
  pinMode(buzzer, OUTPUT);

  delay(500);

  tone(buzzer, 1500);
  delay(100);
  noTone(buzzer);
  delay(100);
  tone(buzzer, 1500);
  delay(100);
  noTone(buzzer);
  delay(100);

  over_time = 0;
}

void loop() {
  struct sps30_measurement m;
  char serial[SPS30_MAX_SERIAL_LEN];
  uint16_t data_ready;
  int16_t ret;

  do {
    ret = sps30_read_data_ready(&data_ready);
    if (ret < 0) {
      Serial.print("error reading data-ready flag: ");
      Serial.println(ret);
    } else if (!data_ready)
      Serial.print("data not ready, no new measurement available\n");
    else
      break;
    delay(100); /* retry in 100ms */
  } while (1);

  ret = sps30_read_measurement(&m);
  if (ret < 0) {
    Serial.print("error reading measurement\n");
  } else {

#ifndef PLOTTER_FORMAT
    Serial.print("PM  1.0: ");
    Serial.println(m.mc_1p0);
    //Serial.print("PM  2.5: ");
    //Serial.println(m.mc_2p5);
    //Serial.print("PM  4.0: ");
    //Serial.println(m.mc_4p0);
    //Serial.print("PM 10.0: ");
    //Serial.println(m.mc_10p0);

#ifndef SPS30_LIMITED_I2C_BUFFER_SIZE
    //Serial.print("NC  0.5: ");
    //Serial.println(m.nc_0p5);
    Serial.print("NC  1.0: ");
    Serial.println(m.nc_1p0);
    //Serial.print("NC  2.5: ");
    //Serial.println(m.nc_2p5);
    //Serial.print("NC  4.0: ");
    //Serial.println(m.nc_4p0);
    //Serial.print("NC 10.0: ");
    //Serial.println(m.nc_10p0);

    //Serial.print("Typical partical size: ");
    //Serial.println(m.typical_particle_size);
#endif

    //Serial.println();

#else
    // since all values include particles smaller than X, if we want to create buckets we 
    // need to subtract the smaller particle count. 
    // This will create buckets (all values in micro meters):
    // - particles        <= 0,5
    // - particles > 0.5, <= 1
    // - particles > 1,   <= 2.5
    // - particles > 2.5, <= 4
    // - particles > 4,   <= 10

    Serial.print(m.nc_0p5);
    Serial.print(" ");
    Serial.print(m.nc_1p0  - m.nc_0p5);
    Serial.print(" ");
    Serial.print(m.nc_2p5  - m.nc_1p0);
    Serial.print(" ");
    Serial.print(m.nc_4p0  - m.nc_2p5);
    Serial.print(" ");
    Serial.print(m.nc_10p0 - m.nc_4p0);
    //Serial.println();


#endif /* PLOTTER_FORMAT */

  }

  int time_interval = 1000;
  int time_left = time_interval;
  int tone_time = 6;
  
  // tuning exponent
  float exponent = 1.3;

  float pm = m.nc_1p0;
  if (pm > 1000) {
    pm = 1000.0;  
  }
  float factor = pow((pm / 1000.0), exponent);
  if (factor < 0.01) {
    factor = 0.01;
  }
  Serial.print("factor = ");
  Serial.print(factor);
  Serial.println();
  int time_gaps = time_left / (pm * factor);
  time_gaps = time_gaps + tone_time;
    // add time for tone duration
  int clicks = time_left / time_gaps;
  Serial.print("time_gaps = ");
  Serial.print(time_gaps);
  Serial.println();
  Serial.print("clicks = ");
  Serial.print(clicks);
  Serial.println();

  // pwm buzzer
  int jitter = tone_time;
  if (time_gaps < time_interval) {
    while (time_left > tone_time) {
      jitter = random(((time_gaps) * (-1)) + tone_time, time_gaps);
      Serial.print("jitter = ");
      Serial.print(jitter);
      Serial.println();
        // add jitter, to make it more natural

      delay(time_gaps + jitter);
        // substract time for tone duration

      // play 3 kHz sound for <tone_time> ms and stop
      tone(buzzer, 3000);
      delay(tone_time);
      noTone(buzzer);
      
      time_left = time_left - (time_gaps + jitter) - tone_time;
    }
  }
  else {
    over_time = over_time + time_gaps - time_interval;
    Serial.print("OVER_TIME = ");
    Serial.print(over_time);
    Serial.println();
    if (over_time > 0) {
      if (over_time < (time_interval * 10)) {
        jitter = random((over_time * (-1)) + tone_time, time_interval + (over_time * 5));
        Serial.print("jitter = ");
        Serial.print(jitter);
        Serial.println();
        delay(over_time + jitter);
        
        // play 3 kHz sound for <tone_time> ms and stop
        tone(buzzer, 3000);
        delay(tone_time);
        noTone(buzzer);

        over_time = 0;
      }
      else {
        delay(time_interval * 10);
        over_time = 0;
        Serial.print("max over_time reached; resetted.");
        Serial.println();
      }
      if ((over_time + jitter) > time_interval) {
        time_left = 0;
      }
      else {
        time_left = time_interval - (over_time + jitter);
        if (time_left > time_interval) {
            time_left = time_interval;
        }
      }
    }
  }

  Serial.print("time_left = ");
  Serial.print(time_left);
  Serial.println();
  if (time_left > 0) {
    delay(time_left);
  }

  Serial.println();
}
